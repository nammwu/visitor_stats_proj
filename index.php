<?php

require 'Slim/Slim.php';

\Slim\Slim::registerAutoloader();

$app = new \Slim\Slim();

$response = $app->response();
$response->header('Access-Control-Allow-Origin', '*');

$app->get('/etc', 'getEtcUser');
$app->get('/portal', 'getPortalUser');
$app->get('/path', 'getPathUser');
// $app->get('/period', 'getPeriodUser');
$app->get('/period/:year', 'getPeriodUser1');
$app->get('/period/:year/:month', 'getPeriodUser2');

// $app->get('/wines/:id', 'getWine');
// $app->get('/wines/search/:query', 'findByName');
// $app->post('/wines', 'addWine');
// $app->put('/wines/:id','updateWine');
// $app->delete('/wines/:id', 'deleteWine');

$app->run();

//년도 선택 시 막대그래프데이터 get
function getPeriodUser1($year) {
	// echo $year;

	// mysqli 트랜잭션
	$link = mysqli_connect("www.3030eng.com","root","Rakgk5go!~","home3030");
	if(mysqli_connect_errno()){
		printf("Connect failed: %s",mysqli_connect_errno());
		exit();
	}
	mysqli_begin_transaction($link);

	//$result = mysqli_query($link, "SELECT DISTINCT referer FROM homepagelog");
	// $result = mysqli_query($link, "SELECT postdate FROM homepagelog");
	//월별집계(중복ip제거)
	$result = mysqli_query($link, "SELECT left(postdate,7), COUNT(distinct userip) FROM homepagelog WHERE left(postdate,4)='". $year ."'GROUP BY left(postdate,7) ");
	mysqli_commit($link);

	//각 변수들 선언 및 카운팅
	$Jan=0; $Feb=0; $Mar=0;
	$Apr=0; $May=0; $Jun=0;
	$Jul=0; $Aug=0; $Sep=0;
	$Oct=0; $Nov=0; $Dec=0;
	$Etc=0;


	while($row = mysqli_fetch_assoc($result)){
		if($year."-01"==$row['left(postdate,7)']){
			$Jan=(int)$row['COUNT(distinct userip)'];
		}
		else if($year."-02"==$row['left(postdate,7)']){
			$Feb=(int)$row['COUNT(distinct userip)'];
		}
		else if($year."-03"==$row['left(postdate,7)']){
			$Mar=(int)$row['COUNT(distinct userip)'];
		}
		else if($year."-04"==$row['left(postdate,7)']){
			$Apr=(int)$row['COUNT(distinct userip)'];
		}
		else if($year."-05"==$row['left(postdate,7)']){
			$May=(int)$row['COUNT(distinct userip)'];
		}
		else if($year."-06"==$row['left(postdate,7)']){
			$Jun=(int)$row['COUNT(distinct userip)'];
		}
		else if($year."-07"==$row['left(postdate,7)']){
			$Jul=(int)$row['COUNT(distinct userip)'];
		}
		else if($year."-08"==$row['left(postdate,7)']){
			$Aug=(int)$row['COUNT(distinct userip)'];
		}
		else if($year."-09"==$row['left(postdate,7)']){
			$Sep=(int)$row['COUNT(distinct userip)'];
		}
		else if($year."-10"==$row['left(postdate,7)']){
			$Oct=(int)$row['COUNT(distinct userip)'];
		}
		else if($year."-11"==$row['left(postdate,7)']){
			$Nov=(int)$row['COUNT(distinct userip)'];
		}
		else if($year."-12"==$row['left(postdate,7)']){
			$Dec=(int)$row['COUNT(distinct userip)'];
		}
		else{
			//$Etc=$row['COUNT(distinct userip)'];
		}
	}


		/*key값 없이 JSON데이터에 담는 방법(user_for_etc.js에서 바로 인덱스 접근해서 테이블에 뿌려주기에 용이하다~)*/
		$arr = array($Jan, $Feb ,$Mar ,$Apr ,$May ,$Jun ,$Jul ,$Aug ,$Sep ,$Oct ,$Nov ,$Dec ,$Etc);
	echo '{"month": ' . json_encode($arr) . '}';

}

//년도,월 선택 시 막대그래프데이터 get
function getPeriodUser2($year, $month) {
	if(1<=$month&&$month<10){
		$month = "0".$month;
	}
	// echo $year."-".$month;

	// mysqli 트랜잭션
	$link = mysqli_connect("www.3030eng.com","root","Rakgk5go!~","home3030");
	if(mysqli_connect_errno()){
		printf("Connect failed: %s",mysqli_connect_errno());
		exit();
	}
	mysqli_begin_transaction($link);

	//$result = mysqli_query($link, "SELECT DISTINCT referer FROM homepagelog");
	// $result = mysqli_query($link, "SELECT postdate FROM homepagelog");
	//월별집계(중복ip제거)
	$result = mysqli_query($link, "SELECT left(postdate,10), COUNT(distinct userip) FROM homepagelog WHERE left(postdate,7)='".$year."-".$month."' GROUP BY left(postdate,10)");
	mysqli_commit($link);

	//각 변수들 선언 및 카운팅
	$day_arr = [];
	//배열초기화
	for($i=0; $i<31; $i++){
		$day_arr[$i]=0;
	}
	//징그러운부분..처리필요
	while($row = mysqli_fetch_assoc($result)){
		if($year."-".$month."-01"==$row['left(postdate,10)']){
			$day_arr[0]=(int)$row['COUNT(distinct userip)'];
		}
		else if($year."-".$month."-02"==$row['left(postdate,10)']){
			$day_arr[1]=(int)$row['COUNT(distinct userip)'];
		}
		else if($year."-".$month."-03"==$row['left(postdate,10)']){
			$day_arr[2]=(int)$row['COUNT(distinct userip)'];
		}
		else if($year."-".$month."-04"==$row['left(postdate,10)']){
			$day_arr[3]=(int)$row['COUNT(distinct userip)'];
		}
		else if($year."-".$month."-05"==$row['left(postdate,10)']){
			$day_arr[4]=(int)$row['COUNT(distinct userip)'];
		}
		else if($year."-".$month."-06"==$row['left(postdate,10)']){
			$day_arr[5]=(int)$row['COUNT(distinct userip)'];
		}
		else if($year."-".$month."-07"==$row['left(postdate,10)']){
			$day_arr[6]=(int)$row['COUNT(distinct userip)'];
		}
		else if($year."-".$month."-08"==$row['left(postdate,10)']){
			$day_arr[7]=(int)$row['COUNT(distinct userip)'];
		}
		else if($year."-".$month."-09"==$row['left(postdate,10)']){
			$day_arr[8]=(int)$row['COUNT(distinct userip)'];
		}
		else if($year."-".$month."-10"==$row['left(postdate,10)']){
			$day_arr[9]=(int)$row['COUNT(distinct userip)'];
		}
		else if($year."-".$month."-11"==$row['left(postdate,10)']){
			$day_arr[10]=(int)$row['COUNT(distinct userip)'];
		}
		else if($year."-".$month."-12"==$row['left(postdate,10)']){
			$day_arr[11]=(int)$row['COUNT(distinct userip)'];
		}

		else if($year."-".$month."-13"==$row['left(postdate,10)']){
			$day_arr[12]=(int)$row['COUNT(distinct userip)'];
		}
		else if($year."-".$month."-14"==$row['left(postdate,10)']){
			$day_arr[13]=(int)$row['COUNT(distinct userip)'];
		}
		else if($year."-".$month."-15"==$row['left(postdate,10)']){
			$day_arr[14]=(int)$row['COUNT(distinct userip)'];
		}
		else if($year."-".$month."-16"==$row['left(postdate,10)']){
			$day_arr[15]=(int)$row['COUNT(distinct userip)'];
		}
		else if($year."-".$month."-17"==$row['left(postdate,10)']){
			$day_arr[16]=(int)$row['COUNT(distinct userip)'];
		}
		else if($year."-".$month."-18"==$row['left(postdate,10)']){
			$day_arr[17]=(int)$row['COUNT(distinct userip)'];
		}
		else if($year."-".$month."-19"==$row['left(postdate,10)']){
			$day_arr[18]=(int)$row['COUNT(distinct userip)'];
		}
		else if($year."-".$month."-20"==$row['left(postdate,10)']){
			$day_arr[19]=(int)$row['COUNT(distinct userip)'];
		}
		else if($year."-".$month."-21"==$row['left(postdate,10)']){
			$day_arr[20]=(int)$row['COUNT(distinct userip)'];
		}
		else if($year."-".$month."-22"==$row['left(postdate,10)']){
			$day_arr[21]=(int)$row['COUNT(distinct userip)'];
		}
		else if($year."-".$month."-23"==$row['left(postdate,10)']){
			$day_arr[22]=(int)$row['COUNT(distinct userip)'];
		}
		else if($year."-".$month."-24"==$row['left(postdate,10)']){
			$day_arr[23]=(int)$row['COUNT(distinct userip)'];
		}
		else if($year."-".$month."-25"==$row['left(postdate,10)']){
			$day_arr[24]=(int)$row['COUNT(distinct userip)'];
		}
		else if($year."-".$month."-26"==$row['left(postdate,10)']){
			$day_arr[25]=(int)$row['COUNT(distinct userip)'];
		}
		else if($year."-".$month."-27"==$row['left(postdate,10)']){
			$day_arr[26]=(int)$row['COUNT(distinct userip)'];
		}
		else if($year."-".$month."-28"==$row['left(postdate,10)']){
			$day_arr[27]=(int)$row['COUNT(distinct userip)'];
		}
		else if($year."-".$month."-29"==$row['left(postdate,10)']){
			$day_arr[28]=(int)$row['COUNT(distinct userip)'];
		}
		else if($year."-".$month."-30"==$row['left(postdate,10)']){
			$day_arr[29]=(int)$row['COUNT(distinct userip)'];
		}
		else if($year."-".$month."-31"==$row['left(postdate,10)']){
			$day_arr[30]=(int)$row['COUNT(distinct userip)'];
		}
		else{
			//$Etc=$row['COUNT(distinct userip)'];
		}
	}


		/*key값 없이 JSON데이터에 담는 방법(user_for_etc.js에서 바로 인덱스 접근해서 테이블에 뿌려주기에 용이하다~)*/
		// $arr = array($Jan, $Feb ,$Mar ,$Apr ,$May ,$Jun ,$Jul ,$Aug ,$Sep ,$Oct ,$Nov ,$Dec ,$Etc);

	echo '{"year_month": ' . json_encode($day_arr) . '}';




}

//아무것도없을때(최초) 막대그래프데이터 get
// function getPeriodUser(){

// 	// mysqli 트랜잭션
// 	$link = mysqli_connect("www.3030eng.com","root","Rakgk5go!~","home3030");
// 	if(mysqli_connect_errno()){
// 		printf("Connect failed: %s",mysqli_connect_errno());
// 		exit();
// 	}
// 	mysqli_begin_transaction($link);

// 	//$result = mysqli_query($link, "SELECT DISTINCT referer FROM homepagelog");
// 	// $result = mysqli_query($link, "SELECT postdate FROM homepagelog");
// 	//월별집계(중복ip제거)
// 	$result = mysqli_query($link, "SELECT left(postdate,7), COUNT(distinct userip) FROM homepagelog GROUP BY left(postdate,7)");
// 	mysqli_commit($link);

// 	//각 변수들 선언 및 카운팅
// 	$Jan=0; $Feb=0; $Mar=0;
// 	$Apr=0; $May=0; $Jun=0;
// 	$Jul=0; $Aug=0; $Sep=0;
// 	$Oct=0; $Nov=0; $Dec=0;
// 	$Etc=0;


// 	while($row = mysqli_fetch_assoc($result)){
// 		if("2016-01"==$row['left(postdate,7)']){
// 			$Jan=(int)$row['COUNT(distinct userip)'];
// 		}
// 		else if("2016-02"==$row['left(postdate,7)']){
// 			$Feb=(int)$row['COUNT(distinct userip)'];
// 		}
// 		else if("2016-03"==$row['left(postdate,7)']){
// 			$Mar=(int)$row['COUNT(distinct userip)'];
// 		}
// 		else if("2016-04"==$row['left(postdate,7)']){
// 			$Apr=(int)$row['COUNT(distinct userip)'];
// 		}
// 		else if("2016-05"==$row['left(postdate,7)']){
// 			$May=(int)$row['COUNT(distinct userip)'];
// 		}
// 		else if("2016-06"==$row['left(postdate,7)']){
// 			$Jun=(int)$row['COUNT(distinct userip)'];
// 		}
// 		else if("2016-07"==$row['left(postdate,7)']){
// 			$Jul=(int)$row['COUNT(distinct userip)'];
// 		}
// 		else if("2016-08"==$row['left(postdate,7)']){
// 			$Aug=(int)$row['COUNT(distinct userip)'];
// 		}
// 		else if("2016-09"==$row['left(postdate,7)']){
// 			$Sep=(int)$row['COUNT(distinct userip)'];
// 		}
// 		else if("2016-10"==$row['left(postdate,7)']){
// 			$Oct=(int)$row['COUNT(distinct userip)'];
// 		}
// 		else if("2016-11"==$row['left(postdate,7)']){
// 			$Nov=(int)$row['COUNT(distinct userip)'];
// 		}
// 		else if("2016-12"==$row['left(postdate,7)']){
// 			$Dec=(int)$row['COUNT(distinct userip)'];
// 		}
// 		else{
// 			//$Etc=$row['COUNT(distinct userip)'];
// 		}
// 	}


// 		/*key값 없이 JSON데이터에 담는 방법(user_for_etc.js에서 바로 인덱스 접근해서 테이블에 뿌려주기에 용이하다~)*/
// 		$arr = array($Jan, $Feb ,$Mar ,$Apr ,$May ,$Jun ,$Jul ,$Aug ,$Sep ,$Oct ,$Nov ,$Dec ,$Etc);
// 	echo '{"month": ' . json_encode($arr) . '}';
// }


function getPortalUser(){
	// $sql = "select * FROM wine ORDER BY name";
	// echo "$app->get('/wines', 'getWines');";

	// mysqli 트랜잭션
	$link = mysqli_connect("www.3030eng.com","root","Rakgk5go!~","home3030");
	if(mysqli_connect_errno()){
		printf("Connect failed: %s",mysqli_connect_errno());
		exit();
	}
	mysqli_begin_transaction($link);

	//각 변수들 선언 및 카운팅
	$portal_naver=0; $portal_daum=0; $portal_google=0;
	$portal_etc=0;
	$total_cnt=0;

	//referer 필드에서 naver를 포함하는 데이터의 갯수(중복제거)
	$result = mysqli_query($link, "SELECT count(distinct referer) FROM homepagelog WHERE referer LIKE '%naver%'");
	mysqli_commit($link);
	$row = mysqli_fetch_assoc($result);
	$portal_naver = (int)$row['count(distinct referer)'];

	//referer 필드에서 daum을 포함하는 데이터의 갯수(중복제거)
	$result = mysqli_query($link, "SELECT count(distinct referer) FROM homepagelog WHERE referer LIKE '%daum%'");
	mysqli_commit($link);
	$row = mysqli_fetch_assoc($result);
	$portal_daum = (int)$row['count(distinct referer)'];
	
	//referer 필드에서 google을 포함하는 데이터의 갯수(중복제거)
	$result = mysqli_query($link, "SELECT count(distinct referer) FROM homepagelog WHERE referer LIKE '%google%'");
	mysqli_commit($link);
	$row = mysqli_fetch_assoc($result);
	$portal_google = (int)$row['count(distinct referer)'];

	//referer 필드에서 기타경로 데이터의 갯수(중복제거)
	$result = mysqli_query($link, "SELECT count(distinct referer) FROM homepagelog WHERE referer NOT LIKE '%naver%' AND referer NOT LIKE '%daum%' AND referer NOT LIKE '%google%'");
	mysqli_commit($link);
	$row = mysqli_fetch_assoc($result);
	$portal_etc = (int)$row['count(distinct referer)'];

	//referer 필드에서 총 데이터의 갯수(중복제거)
	$result = mysqli_query($link, "SELECT count(distinct referer) FROM homepagelog");
	mysqli_commit($link);
	$row = mysqli_fetch_assoc($result);
	$total_cnt = (int)$row['count(distinct referer)'];

	/*key값 없이 JSON데이터에 담는 방법(user_for_etc.js에서 바로 인덱스 접근해서 테이블에 뿌려주기에 용이하다~)*/
	$arr = array($portal_naver, $portal_daum, $portal_google, $portal_etc, $total_cnt);
	echo '{"portal": ' . json_encode($arr) . '}';
}


function getPathUser(){
	// mysqli 트랜잭션
	$link = mysqli_connect("www.3030eng.com","root","Rakgk5go!~","home3030");
	if(mysqli_connect_errno()){
		printf("Connect failed: %s",mysqli_connect_errno());
		exit();
	}
	mysqli_begin_transaction($link);

	//각 변수들 선언 및 카운팅
	$path_search=0; $path_blog=0; $path_cafe=0;
	$path_mail=0; $path_direct=0;
	$total_cnt=0;

	//검색 통해 접속한 데이터의 갯수(중복제거)
	$result = mysqli_query($link, "SELECT count(distinct referer) FROM homepagelog WHERE referer LIKE '%search.naver%' OR referer LIKE '%search.daum%' OR referer LIKE '%google%'");
	mysqli_commit($link);
	$row = mysqli_fetch_assoc($result);
	$path_search = (int)$row['count(distinct referer)'];

	//블로그 통해 접속한 데이터의 갯수(중복제거)
	$result = mysqli_query($link, "SELECT count(distinct referer) FROM homepagelog WHERE referer LIKE '%blog.naver%' OR referer LIKE '%blog.daum%'");
	mysqli_commit($link);
	$row = mysqli_fetch_assoc($result);
	$path_blog = (int)$row['count(distinct referer)'];
	
	//카페 통해 접속한 데이터의 갯수(중복제거)
	$result = mysqli_query($link, "SELECT count(distinct referer) FROM homepagelog WHERE referer LIKE '%cafe.naver%' OR referer LIKE '%map.daum%'");
	mysqli_commit($link);
	$row = mysqli_fetch_assoc($result);
	$path_cafe = (int)$row['count(distinct referer)'];

	//메일 통해 접속한 데이터의 갯수(중복제거)
	$result = mysqli_query($link, "SELECT count(distinct referer) FROM homepagelog WHERE referer LIKE '%mail.naver%' OR referer LIKE '%mail2.daum%' OR referer LIKE '%mail.google%'");
	mysqli_commit($link);
	$row = mysqli_fetch_assoc($result);
	$path_mail = (int)$row['count(distinct referer)'];

	//직접 접속한 데이터의 갯수(중복제거)
	$result = mysqli_query($link, "SELECT count(distinct referer) FROM homepagelog WHERE referer LIKE '%http://www.3030eng.com%'");
	mysqli_commit($link);
	$row = mysqli_fetch_assoc($result);
	$path_direct = (int)$row['count(distinct referer)'];

	//referer 필드에서 총 데이터의 갯수(중복제거)
	$result = mysqli_query($link, "SELECT count(distinct referer) FROM homepagelog");
	mysqli_commit($link);
	$row = mysqli_fetch_assoc($result);
	$total_cnt = (int)$row['count(distinct referer)'];

	/*key값 없이 JSON데이터에 담는 방법(user_for_etc.js에서 바로 인덱스 접근해서 테이블에 뿌려주기에 용이하다~)*/
	$arr = array($path_search, $path_blog, $path_cafe, $path_mail, $path_direct, $total_cnt);
	echo '{"path": ' . json_encode($arr) . '}';
}


function getEtcUser(){
	// $sql = "select * FROM wine ORDER BY name";
	// echo "$app->get('/wines', 'getWines');";

	// mysqli 트랜잭션
	$link = mysqli_connect("www.3030eng.com","root","Rakgk5go!~","home3030");
	if(mysqli_connect_errno()){
		printf("Connect failed: %s",mysqli_connect_errno());
		exit();
	}
	mysqli_begin_transaction($link);

	$result = mysqli_query($link, "SELECT DISTINCT referer FROM homepagelog");
	mysqli_commit($link);

	//각 변수들 선언 및 카운팅
	$search_naver_cnt=0; $search_daum_cnt=0; $search_google_cnt=0;
	$cafe_naver_cnt=0; $cafe_daum_cnt=0;
	$blog_naver_cnt=0; $blog_daum_cnt=0;
	$mail_naver_cnt=0; $mail_daum_cnt=0; $mail_googl_cnt=0;
	$direct_cnt=0;
	$etc_cnt=0;
	$total_cnt=0;

	while($row = mysqli_fetch_assoc($result)){
		if(strpos($row['referer'], 'search.naver')){
			$search_naver_cnt++;
		}
		else if(strpos($row['referer'], 'search.daum')){
			$search_daum_cnt++;
		}
		else if(strpos($row['referer'], 'google')){
			$search_google_cnt++;	
		}

		else if(strpos($row['referer'], 'cafe.naver')){
			$cafe_naver_cnt++;
		}
		else if(strpos($row['referer'], 'map.daum')){
			$cafe_daum_cnt++;
		}

		else if(strpos($row['referer'], 'blog.naver')){
			$blog_naver_cnt++;
		}
		else if(strpos($row['referer'], 'blog.daum')){
			$blog_daum_cnt++;
		}

		else if(strpos($row['referer'], 'mail.naver')){
			$mail_naver_cnt++;
		}
		else if(strpos($row['referer'], 'mail2.daum')){
			$mail_daum_cnt++;
		}
		else if(strpos($row['referer'], 'mail.google')){
			$mail_googl_cnt++;
		}

		else if(strpos($row['referer'], 'http://www.3030eng.com')){
			$direct_cnt++;
		}
		// else if(strpos($row['referer'], '')){
		// 	$etc_cnt++;
		// }
			$total_cnt++;
	}
	/*key값 사용해서 JSON데이터에 담는 방법*/
	  //$arr = array('search_naver_cnt'=> $search_naver_cnt, 'search_daum_cnt'=> $search_daum_cnt, 'search_google_cnt'=> $search_google_cnt,
	// 			 'cafe_naver_cnt'=> $cafe_naver_cnt, 'cafe_daum_cnt'=> $cafe_daum_cnt,
	// 			 'blog_naver_cnt'=> $blog_naver_cnt, 'blog_daum_cnt'=> $blog_daum_cnt,
	// 			 'mail_naver_cnt'=> $mail_naver_cnt, 'mail_daum_cnt'=> $mail_daum_cnt, 'mail_googl_cnt'=> $mail_googl_cnt,
	// 			 'direct_cnt'=> $direct_cnt, 'etc_cnt'=> $etc_cnt, 'total_cnt'=> $total_cnt);

	/*key값 없이 JSON데이터에 담는 방법(user_for_etc.js에서 바로 인덱스 접근해서 테이블에 뿌려주기에 용이하다~)*/
	$arr = array($search_naver_cnt, $search_daum_cnt, $search_google_cnt,
				 $cafe_naver_cnt, $cafe_daum_cnt,
				 $blog_naver_cnt, $blog_daum_cnt,
				 $mail_naver_cnt, $mail_daum_cnt, $mail_googl_cnt,
				 $direct_cnt, $etc_cnt,  $total_cnt);
	echo '{"etc": ' . json_encode($arr) . '}';
}



////아직구현안함//////
function getWines(){
	$sql = "select * FROM wine ORDER BY name";

	echo "$app->get('/wines', 'getWines');";
}

function getWine($id){
	echo "$app->get('/wines/:",$id,", 'getWine');";
}

function findByName($query){
	echo "$app->get('/wines/search/:query', 'findByName');";
}

function addWine(){
	echo "$app->post('/wines', 'addWine');";
}

function updateWine($id){
	echo "$app->put('/wines/:id','updateWine');";
}

function deleteWine($id){
	echo "$app->delete('/wines/:id', 'deleteWine');";
}
?>