    var glob_year=2016;
    var glob_month;

    var rootURL = "http://localhost";   
    get_yearData();
    

    function get_yearData(){
    $.ajax({
      type: 'GET',
      // url: rootURL+'/period',
      url: rootURL+'/period/'+glob_year, //최초생성시에는 최초의 glob_year(=2016)년 데이터 표출하겠다
      dataType: "json",
      success: gen_yearBar_chart
      });
    }

    function get_yearMonthData(){
      $.ajax({
        type: 'GET',
        url: rootURL+'/period/'+glob_year+"/"+glob_month,
        dataType: "json",
        success: gen_yearMonthBar_chart
        });
    }

    // 년도별 bar chart 생성
    function gen_yearBar_chart(data){
      var list = data == null ? [] : (data.month instanceof Array ? data.month : [data.month]);

    $("#ready_state_id").hide();
    $("#show_state_id").show();

      var chart = new CanvasJS.Chart("show_state_id", {

        theme: "theme2",

        title:{
          text: glob_year+"년 (월별)",
          fontSize: 18
        },


        data: [  //array of dataSeries     
        { //dataSeries - first quarter
        /*** Change type "column" to "bar", "area", "line" or "pie"***/    
         type: "column",
         name: "접속자 수",
         color: "#FF8224",
         showInLegend: true,
         dataPoints: [
         { label: "1월", y: list[0] },
         { label: "2월", y: list[1] },
         { label: "3월", y: list[2] },                                    
         { label: "4월", y: list[3] },
         { label: "5월", y: list[4] },
         { label: "6월", y: list[5] },
         { label: "7월", y: list[6] },
         { label: "8월", y: list[7] },
         { label: "9월", y: list[8] },
         { label: "10월", y: list[9] },
         { label: "11월", y: list[10] },
         { label: "12월", y: list[11] }
         ],
         click: function(e){
          glob_month = e.dataPoint.x+1;
          get_yearMonthData();
          // alert(  e.dataSeries.type+ ", dataPoint { x:" + e.dataPoint.x + ", y: "+ e.dataPoint.y + " }" );
         }
        },

      // { //dataSeries - second quarter

      // type: "column",
      // name: "Second Quarter", 
      // showInLegend: true,               
      // dataPoints: [
      // { label: "banana", y: 23 },
      // { label: "orange", y: 33 },
      // { label: "apple", y: 48 },                                    
      // { label: "mango", y: 37 },
      // { label: "grape", y: 20 }
      // ]
      // }
      ],
      /** Set axisY properties here*/
      axisY:{
        //prefix: "$",
        suffix: "명"
      },
      axisX:{
        //prefix: "$",
      }
  });

    chart.render();
  }

  // select box에서 년도 및 월 선택시
  function updateChart(frm) {
    // document.body.style.background = "blue";
    var selected_year = frm.year_select.value;
    var selected_month = frm.month_select.value;

    glob_year = selected_year;
    glob_month = selected_month;

    if(selected_year=="년도" && selected_month=="월"){
      return;
    }
    else if(selected_year!="년도" && selected_month=="월"){
      //년도만 선택한 경우
      get_yearData();
      // $.ajax({
      //   type: 'GET',
      //   url: rootURL+'/period/'+selected_year,
      //   dataType: "json",
      //   success: gen_yearBar_chart
      // });
    }
    else if(selected_year!="년도" && selected_month!="월"){
      //년도와 월 모두 선택한 경우
      get_yearMonthData();
        // $.ajax({
        // type: 'GET',
        // url: rootURL+'/period/'+selected_year+"/"+selected_month,
        // dataType: "json",
        // success: gen_yearMonthBar_chart
        // });
    }

    // document.write(selected_year + "/" + selected_month);

    

    // switch( bank ){
    //  case 0:
    //    frm.bank.value = '은행 및 계좌번호가 표시됩니다.';
    //  break;
    //  case 1:
    //    frm.bank.value = '(국X은행) 0XX-XX-XXXX-XXX';
    //  break;
    //  case 2:
    //    frm.bank.value = '(기X은행) XXX-0XXXXX-0X-0XX';
    //  break;
    //  case 3:
    //    frm.bank.value = '(우X은행) 1XX-XX-0XXXXXXX';
    //  break;
    //  case 4:
    //    frm.bank.value = '(주X은행) 0XXXXXX-0X-0XXXXX';
    //  break;
    // }
  
    // return true;
  }

  function gen_yearMonthBar_chart(data){
      var list = data == null ? [] : (data.year_month instanceof Array ? data.year_month : [data.year_month]);

    // $("#ready_state_id").hide();
    // $("#show_state_id").show();

      var chart = new CanvasJS.Chart("show_state_id", {

        theme: "theme2",

        title:{
          text: glob_year+"년 "+glob_month+"월 (일별)",
          fontSize: 18           
        },


      data: [  //array of dataSeries     
      { //dataSeries - first quarter
       /*** Change type "column" to "bar", "area", "line" or "pie"***/        
       type: "column",
       name: "접속자 수",
       color: "#4374D9",
       showInLegend: true,
       dataPoints: [
       { label: "1", y: list[0] },
       { label: "2", y: list[1] },
       { label: "3", y: list[2] },                                    
       { label: "4", y: list[3] },
       { label: "5", y: list[4] },
       { label: "6", y: list[5] },
       { label: "7", y: list[6] },
       { label: "8", y: list[7] },
       { label: "9", y: list[8] },
       { label: "10", y: list[9] },
       { label: "11", y: list[10] },
       { label: "12", y: list[11] },
       { label: "13", y: list[12] },
       { label: "14", y: list[13] },
       { label: "15", y: list[14] },
       { label: "16", y: list[15] },
       { label: "17", y: list[16] },
       { label: "18", y: list[17] },
       { label: "19", y: list[18] },
       { label: "20", y: list[19] },
       { label: "21", y: list[20] },
       { label: "22", y: list[21] },
       { label: "23", y: list[22] },
       { label: "24", y: list[23] },
       { label: "25", y: list[24] },
       { label: "26", y: list[25] },
       { label: "27", y: list[26] },
       { label: "28", y: list[27] },
       { label: "29", y: list[28] },
       { label: "30", y: list[29] },
       { label: "31", y: list[30] }
       ]
     },

      ],
      /** Set axisY properties here*/
      axisY:{
        //prefix: "$",
        suffix: "명"
      },
      axisX:{
        //prefix: "$",
      }
  });

    chart.render();


  }
