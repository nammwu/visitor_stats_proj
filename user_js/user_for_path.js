    var rootURL = "http://localhost";   
    get_pathData();

    var doughnut_type=0; //명수or비율 선택 시 list의 값들 재활용하기 위해 전역으로 선언(0이면 비율, 1이면 명수)
    var tooltip_type=""; //tooltip 문자열도 "%"일때와, "명"일때 다르게 해줌

    var myDoughnutChart;    

    function get_pathData(){
    $.ajax({
      type: 'GET',
      // url: rootURL+'/period',
      url: rootURL+'/path', 
      dataType: "json",
      success: gen_pathDoughnut_chart
      });
    }

    function gen_pathDoughnut_chart(data){
      var list = data == null ? [] : (data.path instanceof Array ? data.path : [data.path]);

      $("#ready_state_id").hide();
      $("#show_state_id").show();
      // document.write((list[2]/list[4]).toFixed(2)*100+"/"+(list[3]/list[4]).toFixed(2)*100);

      if(doughnut_type==0){
        var data = [
        {
          value: Math.floor((list[0]/list[5])*100),
          color:"#6799FF",
          highlight: "#B2CCFF",
          label: "검색"
        },
        {
          value: Math.floor((list[1]/list[5])*100),
          color: "#F29661",
          highlight: "#FFC19E",
          label: "카페"
        },
        {
          value: Math.floor((list[2]/list[5])*100),
          color: "#BCE55C",
          highlight: "#CEF279",
          label: "블로그"
        },
        {
          value: Math.floor((list[3]/list[5])*100),
          color: "#A566FF",
          highlight: "#D1B2FF",
          label: "메일"
        },
        {
          value: Math.floor((list[4]/list[5])*100),
          color: "#A6A6A6",
          highlight: "#D5D5D5",
          label: "직접접속"
        },
        ];
        tooltip_type="<%= label %> : <%= value %>%";
      }
      else if(doughnut_type==1){
        var data = [
        {
          value: list[0],
          color:"#6799FF",
          highlight: "#B2CCFF",
          label: "검색"
        },
        {
          value: list[1],
          color: "#F29661",
          highlight: "#FFC19E",
          label: "카페"
        },
        {
          value: list[2],
          color: "#BCE55C",
          highlight: "#CEF279",
          label: "블로그"
        },
        {
          value: list[3],
          color: "#A566FF",
          highlight: "#D1B2FF",
          label: "메일"
        },
        {
          value: list[4],
          color: "#A6A6A6",
          highlight: "#D5D5D5",
          label: "직접접속"
        },
        ];
        tooltip_type="<%= label %> : <%= value %>명";
      }

      var ctx = document.getElementById("myChart").getContext("2d");
      myDoughnutChart = new Chart(ctx).Doughnut(data, {
        // animateScale: true
        animationSteps:60, //애니메이션되는 속도, 낮을 수록 빠름
        // showTooltips:true, //tooltip기능 ON, OFF
        //tooltipFillColor: "green", // tooltip배경색깔
        
        /*tooltip자동으로 보이게하는 옵션 설정*/
        // tooltipEvents:[],
        // showTooltips:true,
        // onAnimationComplete: function(){
        //   this.showTooltip(this.segments, true);
        // },
        tooltipTemplate: tooltip_type
      });
    }

    // select box에서 명수/비율 선택시
    function updateChart(frm) {
      // document.body.style.background = "blue";
      var selected_type = frm.type_select.value;

      if(selected_type=="비율"){
        //비율 선택한 경우
        doughnut_type=0;
      }
      else if(selected_type=="명수"){
        //명수 선택한 경우
        doughnut_type=1;
      }
      myDoughnutChart.destroy();
      get_pathData();
    }

    //차트 클릭이벤트 리스너
    $("#myChart").click(
      function(evt){
        $("#main_section").load("user_index_stats/etc.html");
      }
    );